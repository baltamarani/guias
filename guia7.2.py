##Ejercicio 1
# Ejercicio 1.1 
def pertenece(s:list[int], e:int) -> bool:
    for i in range(len(s)):
        if(e==s[i]):
            return True
    return False

#if l.count(e)>0??

def pertenece2(lista:list[int], e:int)-> bool:
    i:int=0
    while i<len(lista):
        if e==lista[i]:
            return True
        i+=1 
        return False
#Ejercicio 1.2    

def divideATodos(lista:list[int], e:int) -> bool:
    for i in range(len(lista)):
        if (i%e==0):
            return True
    return False
#Ejercicio 1.3    
def sumaTotal (lista:list[int])-> int:
    posicion: int = 0
    total:int = 0 
    longitud:int=len(lista)
    while(posicion<longitud):
        valorActual:int=lista(posicion)
        total=valorActual + total 
        posicion+=1
    return total

def sumatotal2 (lista:list[int])->int: 
    contador:int=0    
    for i in range(0,len(lista)):
        contador+=lista[i] 
    return contador 

# Ejercicio 1.4
def ordenados(lista:list[int])-> bool: 
    for i in range (0,len(lista)-1):
        if(not(lista[i]<lista[i+1])):
            return False
        return True 

# Ejercicio 1.5
def tiene_longitud_Mayora7(listapalabras:list[str])-> bool:
    for palabra in listapalabras:
        if len(palabra) > 7:
            return True 
    return False 

# Ejercicio 1.6

def  palindromia(palabra:str) -> bool: 
    l:int = len(palabra) 
    for i in range(0,l//2):
        if (palabra[i] != palabra[(l-1)-i]):
            return False 
    return True    

# Ejercicio 1.7
def colorContraseña (contraseña:str) -> str:
    if (alMenosunaMayuscula_for(contraseña) and alMenosunaMinuscula(contraseña) and longitudMayora8(contraseña) and alMenosunNumero_for(contraseña)):
        return "verde"
    elif longMenor5(contraseña):
        return "Rojo"
    else:
        return "Amarillo"

def longitudMayora8 (frase:str) -> bool:
    return len(frase)>8 

def longMenor5 (frase) -> bool:
    return len(frase) < 5 

def alMenosunaMinuscula(frase:str) -> bool:
    i:int = 0 
    hay_minuscula:bool = False 
    while(i < len(frase) and not hay_minuscula):
        if ('a' <= frase[i] <= 'z'):
            hay_minuscula = True 
            i += 1
        return hay_minuscula

def alMenosunaMinuscula_for(frase:str) -> bool:
    for letra in frase:
        if 'a' <= letra and letra <= 'z':
            return True 
        return False 

def alMenosunaMayuscula_for(frase:str) -> bool:
    for letra in frase:
        if 'A' <= letra and letra <= 'Z':
            return True 
        return False 

x = alMenosunaMayuscula_for("hola")
print(x)

def alMenosunNumero_for(frase:str) -> bool:
    for letra in frase:
        if '0' <= letra and letra <= '9':
            return True 
        return False 
#para elemento elemen, y para posicion range 
####################################################

# Ejercicio 1.8
def saldo_actual(movimientos:list[(str,float)]) -> float:
    saldo:float = 0 
    ingreso : str = "I"
    retiro : str = "R"
    long : int = len(movimientos)
    for i in range(0,long):
        if (movimientos[i][0] == ingreso):
            saldo+= movimientos[i] [1]
        elif (movimientos[i][0] == retiro):
            saldo-= movimientos [i][1]
    return saldo 

#Ejercicio 1.9
def al_menos3_vocales (palabra:str) -> bool:
    contadorDeVocales : int = 0 
    long : int = len(palabra)
    if (pertenece('a',palabra) or pertenece('A',palabra)):
        contadorDeVocales+=1
    if (pertenece('e',palabra) or pertenece('E',palabra)):
        contadorDeVocales+=1
    if (pertenece('i',palabra) or pertenece('I',palabra)):
        contadorDeVocales+=1
    if (pertenece('o',palabra) or pertenece('O',palabra)):
        contadorDeVocales+=1
    if (pertenece('u',palabra) or pertenece('U',palabra)):
        contadorDeVocales+=1
    return contadorDeVocales >= 3 

def al_menos3_vocales2(palabra:str) -> bool:
    vocalesHalladas:list = []
    for vocal in palabra:
        if vocal == 'a' or vocal == 'e' or vocal=='i' or vocal=='o' or vocal=='u'or vocal == 'A' or vocal == 'E' or vocal=='I' or vocal=='O' or vocal=='U':
            vocalesHalladas.append(vocal)
        if vocalesHalladas >= 3:
            return True
        return False  

#Ejercicio 2
#2.1
def modificar_pares(l:list[int]) -> None:
    for i in range (0, len(l), 2): #range (posinicial,posfinal+1, paso)
        l[i] = 0 

def modificar_pares2(l:list[int]) -> None:
    for i in range(0,len(l)):
        if(i%2==0):
            l[i] = 0
    return l 
#2.2
def modificar_pares2(lista:list[int]) -> None:
    output:list=[]
    for i in range(0,len(lista)):
        if(i%2==0):
            output.append(0)
    else:
        output.append(lista[i])
    return output 

#2.3
def quitarVocales(palabra:str) -> str:
    vocales:list= ['a','e', 'i', 'o', 'u', 'A', 'E', 'I', 'O', 'U']
    resultado = []
    for letra in palabra:
        if letra not in vocales:
            resultado.append(letra)
    nueva_palabra = ""
    for letra in resultado:
        nueva_palabra += letra
    return nueva_palabra    
#2.4
def reemplazar_vocales(lista:list[chr]) -> list[chr]:
    vocales:list = ['a','e', 'i', 'o', 'u']
    listaNueva = []
    for letra in lista:
        if letra not in vocales:
            listaNueva.append(letra)
        if letra in vocales:
            listaNueva.append('') 
    return listaNueva

# 2.5
def da_vuelta(lista:list[chr]) -> list[chr]:
    long : int = len(lista)
    lista_nueva : list = []
    for i in range(0,long):
       lista_nueva += lista [long-i-1]
    return lista_nueva

#2.6
def  eliminar_repetidos(palabra:list[chr]) -> list[chr]:
    lista_nueva : list = [] 
    for i in range(0, len(palabra)):
        if (palabra[i] not in lista_nueva):
            lista_nueva.append(palabra[i])
    return lista_nueva

### Ejecicio 3
def aprobado(notas:list[int]) -> int:
    if (notas_mayores_a4(notas) and promedio_de_notas(notas) >= 7):
        return 1
    elif (notas_mayores_a4(notas) and 4<=promedio_de_notas(notas)<7):
        return 2
    elif (not(todasLasNotasSonMayoresQue4(notas)) or promedio(notas)<4):
        return 3


def promedio_de_notas(notas:list[int]) -> float:
    return sumatotal2(notas)/len(notas)

def notas_mayores_a4(notas:list[int]) -> bool:
    for i in notas:
        if i < 4:
            return False
    return True 
### Ejercicio 4
#4.1
def nombre_estudiantes() -> list[str]:
    res : list[str] = []
    nombre = ""
    while(nombre != "Listo"):
        print("ingrese un nombre: ")
        nombre = input()
        if (nombre != 'Listo'):
            res.append(nombre)
    return res 

#4.2 
def historial_Sube() -> list[(str, int)]: 
    resultado:list[(str, int)]= []
    opcion:str=""
    monto:int=0
    plataActual:int=0
    while(opcion != 'X'):
        print("Elegir opcion:('C'= Cargar Credito, 'D'= Descontar Credito, 'X'= Finalizar operacion")
        opcion=input() 
        if(opcion=='C'):
            print("Ingrese un monto: ")
            monto=int(input())
            plataActual+= monto 
            resultado.append((opcion,monto)) 
        elif(opcion=='D'):
            print("Eliga un monto: ")
            plataActual-=monto 
            resultado.append((opcion,monto))
    print ("termino con " + str(plataActual) + " pesos")
    return res 

#4.3
import random
def siete_y_medio() -> list[int]:
    sumaDeCartas: float = 0
    listaCartas:list[int] = []
    opcion: str = "s"
    while (opcion=='s' and sumaDeCartas<=7.5):
        print("----")
        num:int = random.choice([1, 2, 3, 4, 5, 6, 7, 10, 11, 12]) 
        print("te toco un " + str(num))
        listaCartas.append(num) 
       
        if(num == 10 or num == 11 or num== 12):
            sumaDeCartas+=0.5
        else:
            sumaDeCartas+=num 
       
        if(sumaDeCartas>7.5):
            print("sumaDeCartas = "+ str(sumaDeCartas) + " => PERDISTE")
            opcion='X'
        elif (sumaDeCartas == 7.5):
            print("sumaDeCartas = "+ str(sumaDeCartas) + " => GANASTE")
            opcion='X'
        else:
            print("sumaDeCartas = "+ str(sumaDeCartas) + " =>(S para seguir, X para plantarse)")
            opcion=input()  
    return listaCartas 
    
#Ejercio 5
#5.1
def pertenece_a_cada_unoversion_1 (s:[[int]],e:int,res:list[bool]) -> None:
    res.clear() 
    for i in s:
        res.append(pertenece(i,e))
# e= 4
# s=[[1,2,3], [3], [2,3]]
# res=[True, True, True, True, True]

# pertenece_a_cada_unoversion_1(s, e, res)
# print(s)
# print(res)

#5.2
def  pertenece_a_cada_unoversion_2(s:[[int]],e:int,res:list[bool]) -> None:
    res.clear()
    for i in range(0,len(s)):
        res.append(pertenece(s[i],e)) 

## duda de anterior ejercicio

#5.3
def es_matriz(lista:[[int]]) -> bool: 
    if (len(lista)==0) or (len(lista[0])==0):
        return False
    for i in range(len(lista)):
        if len(lista[i]) != len(lista[0]):
            return False 
    return True
#5.4
def filas_ordenadas(m:[[int]], res:list[bool]) -> None:
    res:[bool] = []
    for i in range(0,len(m)):
        if (ordenados(m[i])):
            res.appen(True)
        else:
            res.append(False)
    print (res)  

#5.5
import numpy as np 

def agrandar_matriz(d:int, pot:int) -> [[int]]:
    m = np.random.random((d,d))**2
    nuevamatriz= m
    #si se eleva a 1 imprime m directamente  
    if pot == 1:
        print(m) 
    else:
        incremento=1
    while incremento != pot:
        matrizActualizada = []
        nuevaFila = []
        for fila in range(len(m)):
            if nuevaFila != []:
                matrizActualizada.append(nuevaFila)
                nuevaFila = []
            for columna in range(len(m[fila])):
                n = 0
                elem = 0
                while n!= d:
                    elem += nuevaMatriz[fila][n] * m[n][columna]
                    n += 1 
                    nuevaFila.append(elem) #va creando la nueva fila de la matriz actualizada
            #agrega la ultima fila a matriz actualizada
            matrizActualizada.append(nuevaFila)
            nuevaMatriz = matrizActualizada
            incremento += 1
    print(nuevaMatriz)
    



                               