### PARCIALES ###

# Ejercicio 1
#
#  problema ultima_aparicion (s: seq⟨Z⟩, e: Z) : Z {
#    requiere: {e pertenece a s }
#    asegura: {res es la posición de la última aparición de e en s}
#  }

def ultima_aparicion(s: [int], e:int) -> int:
    for i in range(len(s)):
          if e == s[len(s)-1-i]:
                res = i
    return res
#Por ejemplo, dados
# s = [-1,4,0,4,100,0,100,0,-1,-1]
# e = 0
# print(ultima_aparicion(s, e))
#se debería devolver res=7

#Ejercicio 2
#  problema elementos_exclusivos (s: seq⟨Z⟩, t: seq⟨Z⟩) : seq⟨Z⟩ {
#    requiere: -
#    asegura: {Los elementos de res pertenecen o bien a s o bien a t, pero no a ambas }
#    asegura: {res no tiene elementos repetidos }
#  }
# Por ejemplo, dados
#   s = [-1,4,0,4,3,0,100,0,-1,-1]
#   t = [0,100,5,0,100,-1,5]
# se debería devolver res = [3,4,5] ó res = [3,5,4] ó res = [4,3,5] ó res = [4,5,3]
# ó res = [5,3,4] ó res = [5,4,3]

def elementos_exclusivos (s: [int], t:[int]) -> [int]:
    res:list = []
    for numero in s:
        if not numero in res and not numero in t:
            res.append(numero)
    for numero in t:
      if not numero in s and not numero in res:
           res.append(numero)
    return res

s = [-1,4,0,4,3,0,100,0,-1,-1]
t = [0,100,5,0,100,-1,5]
#print (elementos_exclusivos(s, t))

# Ejercicio 3
# Se cuenta con un diccionario que contiene traducciones de palabras del idioma castellano (claves) a palabras
# en inglés (valores), y otro diccionario que contiene traducciones de palabras en castellano (claves) a palabras
# en alemán (valores). Se pide escribir un programa que dados estos dos diccionarios devuelva la cantidad de
# palabras que tienen la misma traducción en inglés y en alemán.
#  problema contar_traducciones_iguales (ing: dicc⟨String,String⟩, ale: dicc⟨String,String⟩) : Z {
#    requiere: -
#    asegura: {res = cantidad de palabras que están en ambos diccionarios y además tienen igual valor en ambos}

def contar_traducciones_iguales (ingles: dict[str,str], aleman: dict[str,str]) -> int:
    contador:int = 0
    for palabra in ingles.keys():
        if palabra in aleman.keys() and ingles[palabra] == aleman[palabra]:
            contador+=1
    return contador

aleman = {"Mano": "Hand", "Pie": "Fuss", "Dedo": "Finger", "Cara": "Gesicht"}
ingles = {"Pie": "Foot", "Dedo": "Finger", "Mano": "Hand"}
#print(contar_traducciones_iguales(aleman, ingles))
#print(aleman.values())

#Ejercicio 4
# Dada una lista de enteros s, se desea devolver un diccionario cuyas claves sean los valores presentes en s,
# y sus valores la cantidad de veces que cada uno de esos números aparece en s
#  problema convertir_a_diccionario (lista: seq⟨Z⟩) : dicc⟨Z,Z⟩) {
#    requiere: -
#    asegura: {res tiene como claves los elementos de lista y res[n] = cantidad de veces que aparece n en lista}
#  Por ejemplo, dada la lista
#  lista = [-1,0,4,100,100,-1,-1]
#  se debería devolver res={-1:3, 0:1, 4:1, 100:2}

def convertir_a_diccionario (lista: [int]) -> dict:
    diccionario : dict = {}
    for numero in lista:
        if numero in diccionario:
            diccionario[numero] += 1
        else:
            diccionario[numero] = 1
    return diccionario

# lista = [-1,0,4,100,100,-1,-1]
# print(convertir_a_diccionario(lista))

"""--------------------------------------------------------------------------
1) Acomodar [2 puntos]
El próximo 19 de Noviembre se realizará en Argentina la segunda vuelta de las
elecciones presidenciales. En esta competirán solo 2 listas (Lista UP; Lista
LLA). En la mayor parte del país los salones de las escuelas ofician de cuartos
oscuros. En ellos, las autoridades de mesa colocan las boletas sobre los
pupitres. Dado que esta elección se realizará en una eṕoca donde muy
probablemente haga mucho calor, no será raro el caso en el cual las boletas se
vuelen y mezclen a causa de ventiladores prendidos a máxima potencia. Cuando
esto ocurra, las autoridades deberán entrar al cuarto oscuro, juntar todas las
boletas, acomodarlas por partido y volver a distribuirlas en sus lugares.
Implementar una función acomodar que tome una lista con strings que
representan el nombre de lista (UP o LLA) y devuelva una lista con la misma
cantidad de elementos de cada uno de los posibles strings pero agrupadas, las
de Lista UP al principio y las de lista LLA al final.

No está permitido utilizar las funciones sort() y reverse().
problema acomodar (in s: seq<String>) : seq<String> {
    requiere: { Todos los elementos de s son o bien "LLA" o bien "UP"}
    asegura: {|res| = |s|}
    asegura: { Todos los elementos de res son o bien "LLA" o bien "UP"}
    asegura: {res contiene la misma cantidad de elementos "UP" que s}
    asegura: {res contiene todas las apariciones de "UP" antes de las
    apariciones de "LLA"}  """

def acomodar (s:[str]) -> [str]:
    lista_UP:list = []
    lista_LLA:list = []
    for boleta in s:
        if boleta == 'UP':
            lista_UP.append(boleta)
        elif boleta == 'LLA':
            lista_LLA.append(boleta)
        res = lista_UP + lista_LLA
    return res 

s = ["LLA", "UP", "LLA", "LLA", "UP"]
#print(acomodar(s))
#se debería devolver res = ["UP", "UP", "LLA", "LLA", "LLA"]

"""---------------------------------------------------------------------------------------------

Posición umbral [2 puntos]
Durante una noche en un restaurant pasan varios grupos de diversa cantidad de
personas. Para llevar control de esto, el dueño va anotando en su libreta
cuánta gente entra y sale. Para hacerlo rápido decide que la mejor forma de
llevarlo adelante es escribir un número al lado del otro, usando números
positivos para los grupos que entran y negativos para los que salen. Gracias a
estas anotaciones el dueño es capaz de hacer análisis del flujo de clientes.
Por ejemplo, le interesa saber en qué momento de la noche superó una
determinada cantidad de clientes totales que ingresaron (sin importar cuántos
hay en el momento en el local).

Implementar la función pos_umbral, que dada una secuencia de enteros (puede
haber negativos) devuelve la posición en la cual se supera el valor de umbral,
teniendo en cuenta sólo los elementos positivos. Se debe devolver -1 si el
umbral no se supera en ningún momento

problema pos_umbral (in s: seq<Z>, in u: Z) : Z {
    requiere: u ≥ 0
    asegura: {res=-1 si el umbral no se supera en ningún momento }
    asegura: {Si el umbral se supera en algún momento, res es la primera
    posición tal que la sumatoria de los primeros res+1 elementos
    (considerando solo aquellos que son positivos) es estrictamente mayor que
    el umbral u }
Por ejemplo, dadas
s = [1,-2,0,5,-7,3]
u = 5
se debería devolver res = 3 """

def pos_umbral(s: list[int], u:int) -> int:
    sumatoria :int = 0
    for i in range(len(s)):
        if (s[i] > 0):
            sumatoria += s[i]
            if sumatoria > u:
                return i
    return -1

s = [1,-2,0,5,-7,3]
u = 5
#print(pos_umbral(s, u))

"""----------------------------------------------------------------------
3) Columnas repetidas [3 puntos]
Implementar la función columnas_repetidas, que dada una matriz no vacía de m
columnas (con m par y m ≥ 2) devuelve True si las primeras m/2 columnas son
iguales que las últimas m/2 columnas. Definimos a una secuencia de secuencias
como matriz si todos los elementos de la primera secuencia tienen la misma
longitud.

problema columnas_repetidas(in mat:seq<seq<Z>> ) : Bool {
    requiere: {|mat| > 0}
    requiere: {todos los elementos de mat tienen igual longitud m, con m > 0
    (los elementos de mat son secuencias)}
    requiere: {todos los elementos de mat tienen longitud par (la cantidad de
    columnas de la matriz es par)}
    asegura: {(res = true) <=> las primeras m/2 columnas de mat son iguales a
    las últimas m/2 columnas}
Por ejemplo, dada la matriz
m = [[1,2,1,2],[-5,6,-5,6],[0,1,0,1]]
se debería devolver res = true
TIP: para dividir un número entero x por 2 y obtener como resultado un número
entero puede utilizarse la siguiente instrucción: int(x/2) """


def columnas_simetricas (lista:[int]) -> bool:
    primera_mitad = []
    segunda_mitad = []
    for i in range(len(lista)//2):
        primera_mitad.append(lista[i])
    for j in range(len(lista)//2, len(lista)):
        segunda_mitad.append(lista[j])
    return primera_mitad == segunda_mitad

def columnas_repetidas(matriz:list[list[int]]) -> bool:
    for lista in matriz:
        if columnas_simetricas(lista):
            return True
    return False


m = [[1,2,1,2],[-5,6,-5,6],[0,1,0,1]]
#print(columnas_repetidas(m))

"""--------------------------------------------------------------------------------------
4) Rugby 4 naciones [3 puntos]
Desde hace más de 10 años existe en el mundo del rugby un torneo que disputan
anualmente 4 selecciones del sur global (Argentina, Australia, Nueva Zelanda y
Sudáfrica). Este torneo se llama "The rugby championship" o comunmente "4
naciones", ya que suplantó al viejo "3 naciones".

Implementar la función cuenta_posiciones_por_nacion que dada la lista de
naciones que compiten en el torneo, y el diccionario que tiene los resultados
de los torneos anuales en el formato año:posiciones_naciones, donde año es un
número entero y posiciones_naciones es una lista de strings con los nombres de
las naciones, genere un diccionario de naciones:#posiciones, que para cada
Nación devuelva la lista de cuántas veces salió en esa posición.

Tip: para crear una lista con tantos ceros como naciones se puede utilizar la
siguiente sintaxis lista_ceros = [0]*len(naciones)

problema cuenta_posiciones_por_nacion(in naciones: seq<String>, in torneos:
dict<Z,seq<String>>: dict<String,seq<Z>> {
    requiere: {naciones no tiene elementos repetidos}
    requiere: {Los valores del diccionario torneos son permutaciones de la
    lista naciones (es decir, tienen exactamente los mismos elementos que
    naciones, en cualquier orden posible)}
    asegura: {res tiene como claves los elementos de naciones}
    asegura: {El valor en res de una nación es una lista de |naciones|
    elementos que indica en la posición i cuántas veces salió esa nación en la
    i-ésima posición.}
Por ejemplo, dados
naciones= ["arg", "aus", "nz", "sud"]
torneos= {2023:["nz", "sud", "arg", "aus"], 2022:["nz", "sud", "aus", "arg"]}
se debería devolver res = {"arg": [0,0,1,1], "aus": [0,0,1,1], "nz": [2,0,0,0],
"sud": [0,2,0,0]} """

def cuenta_posiciones_por_nacion(naciones: [str], torneos: dict[int, list[str]]) -> dict[str, list[int]]:
    #inicio el diccionario con lista de 0, ej {A: [0,0,0]}
    res: dict = {nacion: [0] * len(naciones) for nacion in naciones}
    # Iteramos sobre cada año en el diccionario 'torneos'
    for año in torneos.keys():
    # Iteramos sobre los índices de la lista de naciones para el año específico
        for i in range(len(torneos[año])):
    # Obtenemos la nación en la posición 'i' en el año 'año'
            nacion = torneos[año][i]
    # Incrementamos el conteo en la posición 'i' para la nación en 'res'
            res[nacion][i] += 1
            
    return res 
# Ejemplo de prueba
naciones= ["arg", "aus", "nz", "sud"]
torneos= {2023:["nz", "sud", "arg", "aus"], 2022:["nz", "sud", "aus", "arg"]}

resultado = cuenta_posiciones_por_nacion(naciones, torneos)
#print(resultado)  # Debería retornar {'A': [1, 1, 1], 'B': [1, 1, 1], 'C': [1, 1, 1]}






