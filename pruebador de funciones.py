from queue import LifoQueue as Pila

def cantidad_elementos (p: Pila) -> int:
    p = Pila ()
    contador_elementos:int = 0 
    pila_temporal: Pila = Pila()
    while (p.empty == False):
        quitador_elementos= p.get()
        pila_temporal.put(quitador_elementos)
        contador_elementos += 1 
    #reconstruccion de pila original
    while (pila_temporal.empty == False):
        p.put(pila_temporal.get()) 
    return contador_elementos


    



