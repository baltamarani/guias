#GUIA 6 
import math

## ejercicio 1
def imprimirholamundo() -> None:
    print("¡Hola mundo!")

print (imprimirholamundo()) 

def raizDe2() -> float:
    return round(math.sqrt (2), 4) 

print (raizDe2()) 

def  factorial_2 () -> int:
    return math.factorial(2) 

def perimetro() -> float:
    return 2*(math.pi)

#Ejercicio 2 
def  imprimir_saludo (nombre: str) -> None:
    print ("hola" + nombre) 


def raiz_cuadrada_de (numero: float) -> float: 
    return math.sqrt(numero)

def fahrenheit_a_celsius(t: float) -> float:
    res = ((t-32)*5)/9 
    return res 

def imprimir_dos_veces(estribillo: str) -> str:  
    print (estribillo*2)  
print(imprimir_dos_veces (estribillo="maya")) 

def es_multiplo_de (n:int, m:int) -> bool:
    return (n%m==0)

def es_par(numero:int)->bool:
    return (es_multiplo_de(numero,2)) 

def  cantidad_de_pizzas(comensales:int, MinCantPorciones:int) -> int: 
    return math.ceil((comensales*MinCantPorciones)/8) 

#Ejercicio 3

def  alguno_es_0(n:float, m:float) -> bool: 
    return (n==0) or (m==0) 

def  ambos_son_0(numero1:float, numero2:float) -> bool: 
    return (numero1==0) and (numero2==0)

def  es_nombre_largo (nombre:str) -> bool: 
    return 3<=len(nombre)<=8 

def  es_bisiesto(año:int) -> bool:
    return (año%400==0) or (año%4==0) and (año%100!=0)

#Ejercicio 4 
def peso_pino (altura:int) ->int:
    if (altura<=3): 
     return 3*altura
    else:
        return 900 + 2*(altura-3)   
    
def es_peso_util(peso:int) -> bool:
    return (peso>=400) and (peso<=1000) 

def sirve_pino (altura:float) -> bool:
    return 1.5<=altura<=4 

def sirve_Pino (altura:int) -> bool:
    return peso_pino(es_peso_util(altura)) 

#Ejercicio 5
def devolver_el_doble_si_es_par(numero:int) -> int: 
    if es_par(numero):
     return (numero*2)
    else:
        return numero 

def  devolver_valor_si_es_par_sino_el_que_sigue(numero:int) -> int:
    if es_par(numero):
     return numero 
    else:
        return numero+1   

def devolver_el_doble_si_es_multiplo3_el_triple_si_es_multiplo9(numero:int) -> int:
    if (es_multiplo_de(numero,3)):
        return numero*2
    elif (es_multiplo_de(numero,9)):
        return numero *3 
    else: 
        return numero 

def lindo_nombre(nombre:str) -> str: 
    if (len(nombre)>=5):
        return "Tu nombre tiene muchas letras"
    else:
        "tu nombre tiene pocos caracteres"

def  elRango(numero:int) -> str:
    if numero<=5:
        return "menor a 5"
    elif 10<=numero<=20:
        return "entre 10 y 20"
    else:
        return "mayo a 20" 
    
#Ejercicio 6

def delUnoalDiez () -> None:
    i = 1 
    while (i<=10):
     print (i)
     i+=1 

def paresdel10al40 () -> None:
    i=10
    while(i<=40):
        print(i)
        i+=2 

def eco() -> None:
    i=1
    while(i<=10):
        print("Eco")
        i+=1

def cuentaRegresiva(numero:int) -> None:
    while(numero>=1): 
        print(numero)
        numero-=1
        print("Despegue") 

def viajeEnELTiempo (añoPartida:int, añoLlegada:int) -> str:
    añoPartida-=1
    while (añoPartida>=añoLlegada):
        print("Usted ha viajado al pasado, esta en el año"+str(añoLlegada))
        añoPartida-=1

#Ejercicio 7 

def unoAldiez() ->None:
    for i in range(1,11,1):
        print(i) 
 

def del10al40 ():
    for i in range (10,41,2):
        print(i) 

def eco2():
    for i in range(10): 
        print("eco")  

def cuentaregresiva2(n:int):
    for i in range(n,0,-1):
        print(i) 
        print("despegue") 

#Ejercicio8
"""
1) X@a= 