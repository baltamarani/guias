from queue import Queue as Cola 

#Ejercicio 1
#1.1
def contar_lineas (nombre_archivo: str) -> str:
    archivo = open("archivoSolo.txt", 'r')
    contenido = archivo.readlines()
    archivo.close()
    return len(contenido)

#print(contar_lineas(open("archivoSolo.txt")))     

#1.2
def existe_palabra(palabra:str, nombre_archivo: str) -> bool:
    archivo = open("archivoSolo.txt", 'r')
    contenido = archivo.readlines()
    for i in range(len(contenido)):
        if palabra in contenido[i]:
            return True
    archivo.close
    return False

#print(existe_palabra("pepe", (open("archivoSolo.txt"))))    

#1.3 
def es_split(frase:str) -> list[str]:
    lista_nueva:list[str] = []
    palabras:str = ""
    for i in frase:
        if i != ' ' and i != '\n':
            palabras += i 
        else:
            if palabras:
                lista_nueva.append(palabras)
            palabras = ""
    if palabras:
        lista_nueva.append(palabras)            
    return lista_nueva

def cant_apariciones(nombre_archivo: str, palabra:str) -> int:
    archivo = open("archivoSolo.txt", 'r')
    contenido = archivo.read()
    palabras_del_contenido = es_split(contenido)
    contador:int = 0
    for palabradelArchivo in palabras_del_contenido:
        if palabra == palabradelArchivo:
            contador+=1
    archivo.close()
    return contador 

#print(cant_apariciones(open("archivoSolo.txt"), "hola"))

# archivo = open("archivoSolo.txt", 'r')
# contenido = archivo.readlines()
# print(contenido[2] [0])

#Ejercicio 2
def es_comentario(linea:str) -> bool:
    i : int = 0
    while (i < len(linea) and linea[i] ==' '):
        i += 1 
    return i < len(linea) and linea[i] == "#"

def clonar_comentarios(nombre_archivo : str) -> None:
    archivo = open("archivoSolo.txt", 'r')
    clon_archivo = open("archivoSolo_clon.txt", 'w')
    contenido = archivo.readlines()
    for linea in contenido:
                if len(linea) > 0 and not(es_comentario(linea)):
                    clon_archivo.write(linea)
    archivo.close()
    clon_archivo.close()

#Ejercicio 3
def  invertir_lineas(nombre_archivo : str) -> None:
    archivo = open("archivoSolo.txt", 'r')
    clon_reverso = open("archivoReverso.txt", 'w')
    contenido = archivo.readlines()
    longitud:int = len(contenido)
    i:int = 0
    while i < longitud:
        ultima_linea = contenido[longitud-1-i]
        clon_reverso.write(ultima_linea)
        i += 1
    archivo.close()
    clon_reverso.close()

#print(invertir_lineas(open("archivoSolo.txt")))

#Ejercicio 4
def agregar_frase_final(nombre_archivo : str, frase : str):
    archivo = open("archivoSolo.txt", 'a')
    archivo.write('\n' + frase)
    archivo.close()
#print(agregar_frase_final("archivoSolo.txt", "nasheeee2"))

#Ejercicio 5
def agregar_frase_principio(nombre_archivo : str, frase : str):
    archivo = open("archivoSolo.txt", 'r')
    lineas = [frase + '\n'] + archivo.readlines()
    archivo.close()
    archivo = open("archivoSolo.txt", 'w')
    for linea in lineas:
        archivo.write(linea)
    archivo.close()
#print(agregar_frase_principio("archivoSolo.txt", "comi tacos"))

#Ejercicio 7 
def datos_de_alumno(datos_csv:str) -> [str]:
    datos_alumno:[str] = []
    dato = ""
    for caracter in datos_csv:
        if caracter == ',':
            datos_alumno.append(dato)
            dato:str = ""
        elif caracter != '\n':
            dato += caracter
    if len(dato) > 0:
        datos_alumno.append(dato)
    return datos_alumno


def promedio_estudiante(nombre_archivo, lu : str) -> float:
    archivoNotas= open("notas.csv", 'r')
    lineas_notas = archivoNotas.readlines()
    suma_notas:int = 0
    cantidad_notas : int = 0
    for linea in lineas_notas:
        if len(linea) > 1:
            datos_alumno:[str] = datos_de_alumno(linea)
            LU_csv: str = datos_alumno[0]
            nota:float = float(datos_alumno[3])

            if lu == LU_csv:
                suma_notas += nota
                cantidad_notas += 1
    archivoNotas.close()
    return (suma_notas/cantidad_notas)

# def calcular_promedio_por_estudiante(archivo_notas : str,archivo_promedios : str):
#     archivoNotas= open("notas.csv", 'r')
#     lineas = archivoNotas.readlines()

#datos = "nro de LU ('915'), materia ('algebra lineal'), fecha ('2/05/24'), nota (9.0, 5.0, 3,5)"
#print(promedio_estudiante("notas.csv", '259'))



#Ejercicio 8
import random
from queue import LifoQueue as Pila 

def generar_nros_al_azar(cantidad : int, desde : int, hasta : int) -> Pila[int]: 
    p:Pila[int] = Pila()
    while cantidad > 0: 
        numeros_al_azar = random.randint(desde, hasta)
        p.put(numeros_al_azar)
        cantidad -= 1
    return p  
    
#Ejercicio 9
def  cantidad_elementos(p : Pila) -> int:
    pila_repuesto = Pila()
    contador:int = 0
    while (p.empty() == False):
        quitador_de_elementos = p.get()
        pila_repuesto.put(quitador_de_elementos)
        contador += 1
    while(pila_repuesto.empty()==False):
        p.put(pila_repuesto.get())
    return contador

#Ejercicio 10
def buscar_el_maximo (p : Pila[int]) -> int:
    maximo= p.get()
    p = Pila()
    pila_temporal:Pila[int] = Pila()
    pila_temporal.put(maximo)
    while (p.empty()==False):
        maximo2 = p.get()
        pila_temporal.put(maximo)
        if maximo < maximo2:
            maximo = maximo2    
    while (pila_temporal.empty() == False):
        p.put(pila_temporal.get()) # rencostruccion de pila original 
    return maximo 
#Ejercicio 11
def esta_bien_balanceada(expresion: str) -> bool:
    res:bool = True 
    p:Pila = Pila ()
    for caracter in expresion:
        p.put(caracter)
    contador_de_parentesis:int = 0 
    while (p.empty()== False):
        quitar_elementos = p.get()
        if quitar_elementos == '(':
            contador_de_parentesis +=1
        if quitar_elementos == ')':
            contador_de_parentesis -= 1
    if contador_de_parentesis != 0:
        res = False 
    return res
#si hay un parentesi que abre lo agrego a la pila, si hay uno cerrado lo saco 
# print(
# esta_bien_balanceada("3*(1x2)-(5-4)"),
# esta_bien_balanceada("7((2x7)"),
# esta_bien_balanceada("8*(9/3))")
# )

#Ejercicio 12
def evaluar_expresion(expresion:str) -> float:
    operandos: Pila = Pila()
    tokens = es_split(expresion)
    print(tokens)
    for token in tokens:
        if '0'<= token <= '9':
            operandos.put(token)
        elif token in ['+', '-', '*', '/']:
            n1= int (operandos.get())
            n2= int (operandos.get())
            
            if token == '+':
                operandos.put(n1+n2)
            if token == '-':
                operandos.put(n1-n2)
            if token == '*':
                operandos.put(n1*n2)
            if token == '/':
                operandos.put(n1/n2)
    return operandos.get()

#print(evaluar_expresion("3 4 + 5 * 2 -"))

#Ejercicio 13
def generar_nros_al_azar_cola(cantidad : int, desde : int, hasta : int) -> Cola[int]:  
    c:Cola[int] = Cola()
    while cantidad > 0:
        numeros_azar = random.randint(desde, hasta)
        c.put(numeros_azar)
        cantidad -= 1
    return c 

#print ((generar_nros_al_azar(5, 1, 10)).queue)

#Ejercicio 14
def cantidad_elementos_cola(c:Cola) -> int:
    cola_repuesto = Cola ()
    contador:int = 0
    while not c.empty():
        quitador_de_elementos = c.get()
        contador += 1
        cola_repuesto.put(quitador_de_elementos)
    while not cola_repuesto.empty():
        c.put(cola_repuesto.get())
    return contador

def copiar_cola_a_lista(c: Cola) -> list:
    elementos = []
    cola_repuesto = Cola()
    # Transfiere los elementos de 'c' a 'cola_repuesto' mientras los copia a la lista
    while not c.empty():
        elemento = c.get()
        elementos.append(elemento)
        cola_repuesto.put(elemento)
    # Restaura los elementos de 'cola_repuesto' a 'c'
    while not cola_repuesto.empty():
        c.put(cola_repuesto.get())
    return elementos

cola = Cola()
cola.put(1)
cola.put(2)
cola.put(3)
# Copia los elementos de la cola original a una lista
estado_inicial = copiar_cola_a_lista(cola)
# Cuenta los elementos de la cola
numero_elementos = cantidad_elementos_cola(cola)
# Copia los elementos de la cola después de contar a una lista
estado_final = copiar_cola_a_lista(cola)
# Verifica que la cola haya quedado en su estado inicial
# print("Número de elementos:", numero_elementos)  # Esto debería imprimir 3
# print("Estado inicial:", estado_inicial)        # Esto debería imprimir [1, 2, 3]
# print("Estado final:", estado_final)            # Esto debería imprimir [1, 2, 3]

#Ejercicio 15
def buscar_maximo_cola(c:Cola[int]) -> int: 
    cola_repuesto:Cola[int] = Cola ()
    maximo = c.get ()
    cola_repuesto.put(maximo)
    while not c.empty():
        maximo2= c.get()
        cola_repuesto.put(maximo2)
        if maximo < maximo2:
            maximo = maximo2
    while not cola_repuesto.empty():
        c.put(cola_repuesto.get())
    return maximo

def copiar_cola_a_lista_2(c: Cola[int]) -> list:
    elementos = []
    cola_repuesto = Cola()
    # Transfiere los elementos de 'c' a 'cola_repuesto' mientras los copia a la lista
    while not c.empty():
        elemento = c.get()
        elementos.append(elemento)
        cola_repuesto.put(elemento)
    # Restaura los elementos de 'cola_repuesto' a 'c'
    while not cola_repuesto.empty():
        c.put(cola_repuesto.get())
    return elementos

cola = Cola()
cola.put(16)
cola.put(700)
cola.put(25)
estado_inicial = copiar_cola_a_lista(cola)
# Cuenta los elementos de la cola
numero_elementos = cantidad_elementos_cola(cola)
# Copia los elementos de la cola después de contar a una lista
estado_final = copiar_cola_a_lista(cola)
# Verifica que la cola haya quedado en su estado inicial
# print("Número de elementos:", buscar_maximo_cola(cola)) 
# print("Estado inicial:", estado_inicial)       
# print("Estado final:", estado_final)       

#Ejercicio 16
def armar_secuencia_bingo() -> Cola[int]:
    lista_numeros = list(range(0, 100))
    random.shuffle(lista_numeros)
    bolillero : Cola = Cola()
    for bolilla in lista_numeros:
        bolillero.put (bolilla)
    return bolillero

def  jugar_carton_de_bingo(carton :[int], bolillero:Cola[int]) -> int:
    bolillero_aux:Cola = Cola()
    contador_jugadas:int = 0
    numeros_sin_marcar:int = len(carton)
    while numeros_sin_marcar > 0:
        sacar_elementos:int = bolillero.get()
        bolillero_aux.put(sacar_elementos)
        if sacar_elementos in carton:
                numeros_sin_marcar -= 1 
        contador_jugadas += 1 
    while not bolillero.empty():
        sacar_elementos = bolillero.get()
        bolillero_aux.put(sacar_elementos)
    return contador_jugadas

# carton = [25, 46, 83, 28, 99]
# bolillero: Cola[int] = armar_secuencia_bingo()
# print(jugar_carton_de_bingo(carton, bolillero))

#Ejercicio 17
def n_pacientes_urgentes(c:Cola[(int,str,str)]) -> int:
    cola_repuesto:Cola = Cola()
    contador:int = 0
    lista_pacientes:list = []
    while not c.empty():
        tupla = c.get()
        cola_repuesto.put(tupla)
        lista_pacientes.append(tupla)
    for i in range(len(lista_pacientes)):
        if lista_pacientes[i][0] in range(0,4): 
            contador += 1
    while not cola_repuesto.empty():
        c.put(cola_repuesto.get())
    return contador 

# c = Cola()
# c.put((5, "juan", "traumatologia"))
# c.put((2, "ana", "cirugia"))
# c.put((7, "fernando", "cardiologia"))
# c.put((1, "susana", "cirugia"))
# print(n_pacientes_urgentes(c))

#Ejercicio 18
#Especificacion
"""requiere: {True}
   asegura: {|res|=|c|}
   asegura: {devuelve una cola ordenada donde si c[i] [3] = true ira primero, seguido de los c[i] [2]= true}  """

def atencion_a_clientes(c:Cola[(str,int,bool,bool)]) -> Cola[(str,int,bool,bool)]:
    cola_prioridad:Cola = Cola()
    cola_preferencial:Cola = Cola() 
    cola_resto_clientes:Cola = Cola() 
    cola_total_repuesto:Cola = Cola () 
    
    while not c.empty():
        cliente:(str,int,bool,bool) = c.get()
        cola_total_repuesto.put(cliente)
        
        if cliente [3] == True:
            cola_prioridad.put(cliente)
        elif cliente [2] == True:
            cola_preferencial.put(cliente)
        else: 
            cola_resto_clientes.put(cliente)
    
    while not cola_total_repuesto.empty():
        c.put(cola_total_repuesto.get())
    
    cola_atencion:Cola= Cola ()
    while not cola_prioridad.empty():
        cola_atencion.put(cola_prioridad.get())
    while not cola_preferencial.empty():
        cola_atencion.put(cola_preferencial.get())
    while not cola_resto_clientes.empty():
        cola_atencion.put(cola_resto_clientes.get())
    return cola_atencion


#Crear una cola de clientes de prueba
# c:Cola[list] = Cola()
# c.put(("Juan Perez", 12345678, False, False))
# c.put(("Ana Gomez", 23456789, True, True))
# c.put(("Carlos Ruiz", 34567890, True, False))
# c.put(("Marta Lopez", 45678901, False, True))
# print(atencion_a_clientes(c).queue)

#Ejercicio 19
"""en diccionarios: pop(clave) ---> me devuelve el valor de esa clave
                    .items() ----> para iterar el diccionario, me devuleve una lista ('clave', 'valor')
                    .clear ---> me borra todo el diccionario
                    .keys ---> devuelve claves (sirve para iterar)
                    .get () ---> devuleve el valor de una clave"""

def agrupar_por_longitud (nombre_archivo: str) -> dict:
    diccionario:dict[int] = {}
    archivo = open('archivoSolo.txt', 'r')
    lineas = archivo.readlines()
    for linea in lineas:
        palabras = es_split(linea)
        for palabra in palabras:
            longitud = len(palabra)
            if longitud in diccionario:
                diccionario[longitud] += 1
            else:
                diccionario[longitud] = 1
    return diccionario

#print(agrupar_por_longitud("archivoSolo.txt"))

#Ejercicio 20
# def calcular_promedio_por_estudiante_dic (nombre_archivo_notas:str) -> dict[str, float]:

#Ejercicio 22
historiales:dict[str, Pila] = {}

def  visitar_sitio(historiales:dict[str, Pila[str]], usuario:str ,sitio:str):
    historial:Pila = Pila ()
    historial.put(sitio)
    historiales[usuario] = historial 

def navegar_hacia_atras(historiales: dict[str, Pila[str]],usuario:str):
    if usuario in historiales and not historiales[usuario].empty():
        historiales[usuario].get()

historiales = {}
visitar_sitio(historiales, "Usuario1", "google.com")
visitar_sitio(historiales, "Usuario1", "facebook.com")
navegar_hacia_atras(historiales, "Usuario1")
visitar_sitio(historiales, "Usuario2", "youtube.com")

# for usuario, historial in historiales.items():
#     print(f"{usuario}: {historial.queue}")

#Ejercicio 23
def agregar_producto(inventario:dict,nombre:str, precio:float,cantidad:int):
    if not nombre in inventario:
        inventario[nombre] = {
            "precio": precio,
            "cantidad": cantidad}

def actualizar_stock(inventario:dict,nombre:str, cantidad:int):
    if nombre in inventario:
        inventario[nombre] ["cantidad"] = cantidad
    else:
        print("no esta disponible")    

def actualizar_precio(inventario:dict,nombre:str, precio:float):
    if nombre in inventario:
        inventario[nombre] ["precio"] = precio

def  calcular_valor_inventario(inventario:dict) -> float:
    precio_X_cantidad= []
    contenido = inventario.items()
    for producto, informacion in contenido:
        precio_X_cantidad.append(informacion["cantidad"] * informacion["precio"])
    return sum(precio_X_cantidad)

# inventario:dict = {}
# agregar_producto(inventario, "camisa", 15.0, 30)
# agregar_producto(inventario, "jordan travis", 600.0, 8)
# agregar_producto(inventario, "camiseta arg seleccion", 50.0, 50)
# actualizar_precio(inventario, "camisa", 10.0)
# valorTotal = calcular_valor_inventario(inventario)
# print("El valor de todos los productos es:", valorTotal)



