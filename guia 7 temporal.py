# def pertenece (seq:list[int], e: int) -> bool:
#     for i in range(0, len(seq)):
#         if e == seq[i]:
#             return True 
#     return False 

# def pertenece2(seq: list[int], e:int) -> bool:
#     i: int = 0
#     while i < len(seq):
#         if e == seq[i]:
#             return True
#         i += 1
#     return False 

# #1.2
# def divide_a_todos (seq:list[int], e: int) -> bool:
#     i : int = 0
#     res: bool = True 
#     while i < len(seq):
#         if (seq[i] % e != 0):
#             res = False 
#         i +=1
#     return res

# def divide_a_todos2 (seq:list[int], e: int) -> bool:
#     for num in seq:
#         if num % e != 0:
#             return False 
#     return True   

#1.3
def suma_total (lista: [int]) -> int: 
    contador:int = 0
    i : int = 0
    for numero in lista:
        contador += numero
    return contador 

#1.4
def ordenados (lista:[int]) -> bool: 
    for i in range(len(lista)-1):
        if lista[i] > lista[i+1]:
            return False 
    return True

#1.5
def len_palabra_mayor_a_7(palabra:str) -> bool:
    if len(palabra) > 7:
        return True 
    return False

#1.6
def es_palindromo (palabra:str) -> bool:
    longitud : int = len(palabra)
    for i in range(longitud//2):
        if palabra[i] != palabra[(longitud-1)-i]:
            return False
    return True

#1.7
def hay_minusculas (palabara:str) -> bool:
    for i in range(len(palabara)):
        if ('a' <= palabara[i] <='z'):
            return True 
    return False

def hay_mayusculas (palabara:str) -> bool:
    for i in range(len(palabara)):
        if ('A' <= palabara[i] <= 'Z'):
            return True 
    return False

def hay_numeros (palabara:str) -> bool:
    for i in range(len(palabara)):
        if ('0'<= palabara[i] <= '9'):
            return True 
    return False

def fortaleza_contraseña(contraseña:str) -> None: 
    longitud = len(contraseña)
    if (longitud > 8 ) and (hay_minusculas(contraseña)) and (hay_mayusculas(contraseña)) and (hay_numeros(contraseña)):
        print ('VERDE')
    elif (longitud > 5 ):
        print ('ROJA')
    else:
        print('AMARILLA') 

def movimientos_bancarios (transacciones:list[(str,int)]) -> int:
    saldoInicial: int = 0 
    total: int = 0 
    ingreso:str = 'I'
    retiro:str = 'R'
    longitud:int = len(transacciones)
    for i in range(longitud):
        if transacciones[i] [0] == ingreso:
            total+= transacciones [i] [1]
        elif transacciones [i] [0] == retiro:
            total -= transacciones [i] [1] 
    return total 

#1.9 
def al_menos_3_vocales(palabra:str) -> bool:
    vocales: list = ['a', 'e', 'i', 'o', 'u', 'A', 'E', 'I', 'O', 'U']
    vocales_de_la_palabra:[str] = []
    sin_vocales_repetidas = []
    for i in range (len(palabra)):
        if palabra[i] in vocales:
            vocales_de_la_palabra.append(palabra[i])
    for vocal in vocales_de_la_palabra:
                if vocal not in sin_vocales_repetidas:
                    sin_vocales_repetidas.append(vocal)
                if len(sin_vocales_repetidas) >= 3: 
                    return True 
    return False 

# print(al_menos_3_vocales("flama")) 

#Ejercicio 2 
#2.1
def modificar_pares_inout(l:list[int]) -> None:
    for i in range (0, len(l)): #range (posinicial,posfinal+1, paso)
        if (l[i]%2 == 0):
            l[i] = 0  

# lista = [1, 2, 4, 6, 8, 9]
# modificar_pares (lista)
# print(lista)

#2.2
def modificar_pares_in(l:list[int]) -> None:
    lista_nueva:[int] = []
    for i in range (0, len(l)): #range (posinicial,posfinal+1, paso)
        if (l[i]%2 == 0):
            lista_nueva.append(0)
        else: 
            lista_nueva.append(l[i]) 
    return lista_nueva

#2.3
def borrar_vocal(frase:str) -> str:
    vocales: list = ['a', 'e', 'i', 'o', 'u', 'A', 'E', 'I', 'O', 'U']
    frase_nueva:str = ""
    for i in range(len(frase)):
        if frase[i]not in vocales:
            frase_nueva += frase[i]
    return frase_nueva

#print(borrar_vocal("hola como estas")) 

#2.4 
def reemplaza_vocales(lista:[chr]) -> [chr]: 
    vocales:list = ['a', 'e', 'i', 'o', 'u']
    lista_nueva:list = []
    for letra in lista:
        if letra in vocales:
            lista_nueva.append('_')
        else:
            lista_nueva.append(letra)
    return lista_nueva
#print (reemplaza_vocales(['g', 'a', 't', 'o']))  

#2.5
def dar_vuelta_str(lista:list[chr]) -> [chr]:
    lista_nueva: [chr] = []
    longitud = len(lista)
    for i in range(len(lista)):
        lista_nueva.append(lista[(longitud-1)-i])  
    return lista_nueva
#print(dar_vuelta_str("radar"))

#2.6
def eliminar_repetidos(lista:list[chr]) -> [chr]:
    lista_sin_repetidos:[chr] = []
    for caracter in range(len(lista)):
        if lista[caracter] not in lista_sin_repetidos:
            lista_sin_repetidos.append(lista[caracter])
    return lista_sin_repetidos
#print(eliminar_repetidos("elefante")) 

# Ejercicio 3 
def notas_mayores_a_4(lista:[int]) -> bool: 
    for i in range (len(lista)):
        if lista[i] < 4:
            return False 
    return True 

def promedio_notas(lista:[int]) -> float:
    canttidadNotas:int=0
    while canttidadNotas < len(lista):
        suma = suma_total(lista)
        canttidadNotas += 1
    return (suma/canttidadNotas) 

#print(promedio_notas([6, 6, 8, 3]))

def aprobado (notas:[int]) -> int:
    if (promedio_notas(notas) >= 7) and notas_mayores_a_4(notas):
        return 1
    elif (4 <= promedio_notas(notas) < 7) and notas_mayores_a_4:
        return 2
    else:
        return 3 
    
#Ejercicio 4
#4.1
def nombres_de_mis_estudiantes () -> [str]:
    res:[str] = []
    nombres = ""
    while nombres != "listo":
        print("Ingresar nombres: ")
        nombres = input()
        if nombres != 'listo':
            res.append(nombres)
    return res 

#print(nombres_de_mis_estudiantes) 

#4.2
def historial_de_sube () -> list[(chr, int)]:
    res:[(chr, int)] = []
    monto:int = 0
    operacion = ""
    plataActual:int = 0 
    
    while operacion != 'X':
        print("Eliga la operacion a realizar: 'C'= cargar credito, 'D'= descontar credito, 'X' = cerrar")
        operacion = input()
        monto:int = input()
        
        if operacion == 'C':
            print("Proporcione el monto a cargar: ")
            plataActual += monto
            res.append((operacion, monto))
        
        elif operacion == 'D':
            print ("Proporcione el monto a descontar: ")
            plataActual-= monto 
            res.append((operacion, monto))
        
        else:
            print("Usted ha realizado la operacion con exito!, su dinero actual es" + str(plataActual) + "pesos") 
    return res

#4.3




             
